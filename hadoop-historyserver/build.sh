#!/bin/sh

echo "# ##################################################"
echo "     Building hadoop-historyserver image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-historyserver:2.9.1 .
