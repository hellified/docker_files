#!/bin/sh

echo "# ##################################################"
echo "     Building hadoop-namenode-checkpoint image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-namenode-checkpoint:2.9.1 .
