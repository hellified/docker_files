#!/bin/sh

echo "# ##################################################"
echo "         Building archlinux image"
echo "# ##################################################"
docker build --rm -t hellified/archlinux .
