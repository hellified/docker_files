#!/bin/sh

echo "# ##################################################"
echo "        Building hadoop-datanode image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-datanode:2.9.1 .
