#!/bin/bash

# Waits until a Docker image is started
# $1 - docker image name
# $2 - max number of seconds

retry() {
  local -r -i max_attempts="$2"
  local attempt_num=0
  until [ "$(docker inspect -f {{.State.Health.Status}} $1 )" == "healthy" ]; do
      if (( attempt_num++ == max_attempts ))
        then
          echo "$1 not started after $attempt_num seconds!"
          return 1
        else
          sleep 1;
      fi
  done;
}

declare -fxr retry
retry "$1" "$2"
