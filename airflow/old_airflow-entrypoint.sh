#!/bin/bash

#export AIRFLOW_HOME=/usr/local/airflow
source hadoop-setup.sh

airflow initdb

#Start the scheduler
airflow scheduler --daemon # --run-duration 86400
# worker
# flower
airflow webserver
