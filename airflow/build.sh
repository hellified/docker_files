#!/bin/sh

echo "# ##################################################"
echo "         Building airflow image"
echo "# ##################################################"
docker build --rm -t hellified/airflow .
