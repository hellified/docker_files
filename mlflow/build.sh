#!/bin/sh

echo "# ##################################################"
echo "         Building anaconda image"
echo "# ##################################################"

docker build --rm -t hellified/anaconda:latest .
