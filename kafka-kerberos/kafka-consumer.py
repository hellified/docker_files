from confluent_kafka import Consumer, KafkaError

c = Consumer({
    'bootstrap.servers': 'hdp-prod1-kafka7.sth.kambi.com:9092',
    'sasl.kerberos.principal': 'dwhdeveloper@SERVICES.KAMBI.COM',
    'sasl.kerberos.keytab': '/etc/security/keytabs/dwhdeveloper.keytab',
    'sasl.kerberos.service.name': 'kafka',
    'security.protocol': 'SASL_PLAINTEXT',
    'sasl.mechanisms': 'GSSAPI',
    'group.id': 'andros',
    'enable.auto.commit': 'false',
    'debug': 'consumer',
    'default.topic.config': {
      'auto.offset.reset': 'latest'
    }
  })

c.subscribe(['data.player.frontend.PunterLogin'])

while True:
    msg = c.poll(1.0)

    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue

    print('Received message: {}'.format(msg.value().decode('utf-8')))

c.close()