# docker \
#     run -i -t \
#     --rm \
#     -v /etc/krb5.conf:/etc/krb5.conf \
#     -v /etc/security/keytabs/dwhdeveloper.keytab:/etc/security/keytabs/dwhdeveloper.keytab \
#     kafka-consumer \
#     /bin/bash 

docker \
    run -i -t \
    --rm \
    -v /etc/krb5.conf:/etc/krb5.conf \
    -v /home/isahod/devel/keytabs/dwhdeveloper.keytab:/etc/security/keytabs/dwhdeveloper.keytab \
    hellified/kafka-kerberos \
    /bin/bash 
