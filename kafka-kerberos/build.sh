#!/bin/sh

echo "# ##################################################"
echo "         Building kafka-kerberos image"
echo "# ##################################################"
docker build -t isahod/kafka-kerberos:latest .
