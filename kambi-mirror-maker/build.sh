#!/bin/sh

echo "# ##################################################"
echo "         Building base mirror maker"
echo "# ##################################################"
docker build --rm -t hellified/mirror_maker .
