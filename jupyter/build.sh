#!/bin/sh

echo "# ##################################################"
echo "         Building jupyter image"
echo "# ##################################################"
docker build --rm -t hellified/jupyter .
