#!/bin/sh

echo "# ##################################################"
echo "         Building hadoop-spark image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-spark:2.2.2_2.9.1 .
