#!/bin/sh

echo "# ##################################################"
echo "        Building hadoop-nodemanager image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-nodemanager:2.9.1 .
