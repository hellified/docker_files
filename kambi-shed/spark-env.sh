PERSONAL_ENV_FILE="$HOME/conda_envs/jupyter.notebooks.env"
if [ -f "${PERSONAL_ENV_FILE}" ]; then
   . "${PERSONAL_ENV_FILE}"
fi

PYTHONPATH=/usr/bin/python3

export PYTHONPATH="${PYTHONPATH}"
export PYSPARK_PYTHON="${PYTHONPATH}/python"
export SPARK_HOME="/usr/hdp/current/spark2-client"
export PYLIB="$SPARK_HOME/python/lib"
export PYTHONPATH="$PYTHONPATH:$PYLIB/py4j-0.10.4-src.zip"
export PYTHONPATH="$PYTHONPATH:$PYLIB/pyspark.zip"
export PYSPARK_SUBMIT_ARGS="--name yarn pyspark-shell"
