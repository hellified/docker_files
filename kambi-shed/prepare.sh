#!/bin/bash -e
SOURCE_BOX=hdp-jupyter02-prod.bdp.kambi.com
#hdp-prod1-jupyter3.sth.kambi.com
# SOURCE_BOX=hdp-prod1-gw1.sth.kambi.com

# Environment files
if [ -d env_files ]
then
    rm -rf env_files.old
    mv env_files env_files.old
fi
mkdir env_files
scp -r $SOURCE_BOX:/etc/bdp/conf/env_files .

# Hadoop configuration files
# if [ -d hadoop-conf ]
# then
#     rm -rf hadoop-conf.old
#     mv hadoop-conf hadoop-conf.old
# fi
# mkdir hadoop-conf
# cp -r hadoop_conf/* hadoop-conf
