#!/bin/sh

echo "# ##################################################"
echo "         Building kafdrop image"
echo "# ##################################################"
docker build --rm -t hellified/kfdrop .
