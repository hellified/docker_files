#!/bin/bash -e
source $HOME/devel/common/bash/bash_common.sh
registry=localhost:1234

build_args=''

while IFS=, read -r username password access_key_id  secret_access_key console_login_link; do
    # do something...
    if [[ "$username" != "User name" ]]; then
        build_arguments['AWS_ACCESS_KEY_ID']=$access_key_id
        build_arguments['AWS_SECRET_KEY']=$secret_access_key
        build_arguments['AWS_REGION']=eu-north-1
    fi

done < $HOME/.secrets/aws_creds.csv;

date_stamp=`date +%Y-%m-%d`
echo $date_stamp

declare -A build_arguments

build_arguments['AIRFLOW_VERSION']='2.2.0'
build_arguments['COMMONS_HTTPCLIENT_VERSION']='3.1'
build_arguments['ELASTICSEARCH_HADOOP_VERSION']='7.6.1'
build_arguments['HADOOP_VERSION']='3.3.1'
build_arguments['HDP_VERSION']='3.1.5'
build_arguments['JAVA_VERSION']='17'
build_arguments['JAVAX_ACTIVATION_VERSION']='1.1.1'

build_arguments['KAFKA_LIB_VERSION']='0-10'
build_arguments['KAFKA_VERSION']='2.8.1'

build_arguments['MAVEN_MAJOR_VERSION']='3'
build_arguments['MAVEN_VERSION']='3.8.4'

build_arguments['MLFLOW_VERSION']='0.7.0'

build_arguments['MONGO_HADOOP_SPARK_VERSION']='2.0.2'
build_arguments['MONGO_JAVA_VERSION']='4.0.1'

build_arguments['PYTHON_MAJOR_VERSION']='3'
build_arguments['PYTHON_VERSION']='3.10'

build_arguments['SCALA_VERSION']='2.12'
build_arguments['SPARK_HADOOP_VERSION']='3.2'
build_arguments['SPARK_VERSION']='3.2.1'

if [[ "isaac" == $USER ]]
then
    prefix='hellified'
    build_arguments['UBUNTU_VERSION']='20.04'
else
    prefix=$USER
    build_arguments['UBUNTU_VERSION']='20.04'
fi

build_arguments['PREFIX']=$prefix

# Define containers to be built based on if I'm at work or not
docker_files=(
)

if [[ "isaac" == $USER ]]
then
    docker_files+=(
        ubuntu \
        archlinux \
        cert-watcher
    )
fi

if [[ "isaac" != $USER ||  ${WORKING+x} ]]
then
    docker_files+=(
        kambi-spark \
        kambi-mirror-maker \
        kafka-s3
    )
fi

#kafka-mirror_maker \
    # kafka-kerberos \
    #     kambi-jupyter \
    #     kambi-shed
    # kafka-connect \
    # hadoop \
    # hadoop-spark \
    # hadoop-namenode \
    # hadoop-namenode-checkpoint \
    # hadoop-datanode \
    # hadoop-resourcemanager \
    # hadoop-nodemanager \
    # hadoop-historyserver \


for argument_name in "${!build_arguments[@]}"
do
    build_args+=" --build-arg ${argument_name}=${build_arguments[$argument_name]}"
done


# #############################################
echo "Building:"
for image_name in "${docker_files[@]}"
do
    echo $image_name
done

for image_name in "${docker_files[@]}"
do
    red "# ######################################"
    red "# Building $image_name"
    red "# ######################################"
    (
        echo "Prefix: [$prefix]"
        cd $image_name \
          && if [ -f ./prepare.sh ]; then purple "# Executing prepare script."; ./prepare.sh;  fi \
          && docker build --rm $build_args \
                    -t $prefix/$image_name:latest .
    )
                    # -t $prefix/$image_name:${date_stamp} \

done


# # Build AWS friendly containers
# aws_docker_files=(
#     kafka-connect
# )

# for image_name in "${aws_docker_files[@]}"
# do
#     red "# ######################################"
#     red "# Building $image_name"
#     red "# ######################################"
#     ( cd $image_name \
#           && if [ -f ./prepare.sh ]; then purple "# Executing prepare script."; ./prepare.sh;  fi \
#           && docker build --rm $build_args \
#                     --secret id=aws_access_key,src=$HOME/.secrets/aws_access_key \
#                     --secret id=aws_secret_access_key,src=$HOME/.secrets/aws_secret_access_key \
#                     -t $prefix/$image_name:latest .)
#                     # -t $prefix/$image_name:${date_stamp} \

# done
