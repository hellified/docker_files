#!/bin/sh

echo "# ##################################################"
echo "        Building hadoop-namenode image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-namenode:2.9.1 .
