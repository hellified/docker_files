#!/bin/sh

echo "# ##################################################"
echo "         Building arch shed image"
echo "# ##################################################"
docker build --rm -t hellified/arch_shed .
