#!/bin/sh

echo "# ##################################################"
echo "         Building hadoop image"
echo "# ##################################################"
docker build -t hellified/hadoop:2.9.1 .
