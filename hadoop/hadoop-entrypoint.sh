#!/bin/bash
source /hadoop-setup.sh

# if [ -n "$GANGLIA_HOST" ]; then
#     mv /etc/hadoop/hadoop-metrics.properties /etc/hadoop/hadoop-metrics.properties.orig
#     mv /etc/hadoop/hadoop-metrics2.properties /etc/hadoop/hadoop-metrics2.properties.orig

#     for module in mapred jvm rpc ugi; do
#         echo "$module.class=org.apache.hadoop.metrics.ganglia.GangliaContext31"
#         echo "$module.period=10"
#         echo "$module.servers=$GANGLIA_HOST:8649"
#     done > /etc/hadoop/hadoop-metrics.properties

#     for module in namenode datanode resourcemanager nodemanager mrappmaster jobhistoryserver; do
#         echo "$module.sink.ganglia.class=org.apache.hadoop.metrics2.sink.ganglia.GangliaSink31"
#         echo "$module.sink.ganglia.period=10"
#         echo "$module.sink.ganglia.supportsparse=true"
#         echo "$module.sink.ganglia.slope=jvm.metrics.gcCount=zero,jvm.metrics.memHeapUsedM=both"
#         echo "$module.sink.ganglia.dmax=jvm.metrics.threadsBlocked=70,jvm.metrics.memHeapUsedM=40"
#         echo "$module.sink.ganglia.servers=$GANGLIA_HOST:8649"
#     done > /etc/hadoop/hadoop-metrics2.properties
# fi

case $HOST_RESOLVER in
    "")
        echo "No host resolver specified. Using distro default. (Specify HOST_RESOLVER to change)"
        ;;

    files_only)
        echo "Configure host resolver to only use files"
        configureHostResolver files
        ;;

    dns_only)
        echo "Configure host resolver to only use dns"
        configureHostResolver dns
        ;;

    dns_files)
        echo "Configure host resolver to use in order dns, files"
        configureHostResolver dns files
        ;;

    files_dns)
        echo "Configure host resolver to use in order files, dns"
        configureHostResolver files dns
        ;;

    *)
        echo "Unrecognised network resolver configuration [${HOST_RESOLVER}]: allowed values are files_only, dns_only, dns_files, files_dns. Ignoring..."
        ;;
esac

exec $@
