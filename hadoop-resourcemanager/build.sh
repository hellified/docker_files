#!/bin/sh

echo "# ##################################################"
echo "        Building hadoop-resourcemanager image"
echo "# ##################################################"

docker build --rm -t hellified/hadoop-resourcemanager:2.9.1 .
